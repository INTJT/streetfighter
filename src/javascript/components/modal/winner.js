import { createElement } from "../../helpers/domHelper";
import { createFighterImage } from "../fighterPreview";
import { showModal } from "./modal";

export function showWinnerModal(fighter) {

  const bodyElement = createElement('div');

  const title = createElement({ tagName: 'h3' });
  title.innerHTML = `${fighter.name.toUpperCase()} WINS`;

  const phase = createElement({ tagName: 'p' });
  phase.innerHTML = "You did quite well, but you need more training to defeat me!";

  bodyElement.appendChild(title);
  bodyElement.appendChild(createFighterImage(fighter));
  bodyElement.appendChild(phase);

  showModal({
    title: "KO", 
    bodyElement,
    onClose() { window.location.reload(); }
  });

}
