import { controls } from '../../constants/controls';

const TIMEOUT = 10 * 1000;

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {

    function createPlayer(fighter) {

      return {

        __proto__: fighter,

        current_health: fighter.health,
        canAttack: true,
        canSpecialAttack: true,
        blocked: false,
        combination: new Set(),

        get health() { return this.current_health; },
        set health(value) {

          this.current_health = Math.max(0, value);

          const position = this === playerOne ? "left" : "right";
          const procents = 100 * this.current_health / this.__proto__.health;

          document.getElementById(`${position}-fighter-indicator`)
            .style.setProperty("width", `${procents}%`);

          if (this.current_health <= 0) ko(this);

        }

      };

    }

    const playerOne = createPlayer(firstFighter);
    const playerTwo = createPlayer(secondFighter);
  
    function keyDown(event) {

      switch(event.code) {

        case controls.PlayerOneAttack:
          if (!playerOne.blocked && playerOne.canAttack) {
            playerOne.canAttack = false;
            if (!playerTwo.blocked) {
              playerTwo.health -= getDamage(playerOne, playerTwo);
            }
          }
          break;

        case controls.PlayerTwoAttack:
          if (!playerTwo.blocked && playerTwo.canAttack) {
            playerTwo.canAttack = false;
            if(!playerOne.blocked) {
              playerOne.health -= getDamage(playerTwo, playerOne);
            }
          }
          break;
        
        case controls.PlayerOneBlock:
          playerOne.blocked = true;
          break;

        case controls.PlayerTwoBlock:
          playerTwo.blocked = true;
          break;

        default:
          if (controls.PlayerOneCriticalHitCombination.includes(event.code) && !playerOne.blocked && playerOne.canSpecialAttack) {
            playerOne.combination.add(event.code);
            if (playerOne.combination.size === controls.PlayerOneCriticalHitCombination.length) {
              playerOne.canSpecialAttack = false;
              setTimeout(() => {
                playerOne.canSpecialAttack = true;
              }, TIMEOUT);
              playerTwo.health -= getSpecialHitPower(playerOne);
            }
          } else if (controls.PlayerTwoCriticalHitCombination.includes(event.code) && !playerTwo.blocked && playerTwo.canSpecialAttack) {
            playerTwo.combination.add(event.code);
            if (playerTwo.combination.size === controls.PlayerTwoCriticalHitCombination.length) {
              playerTwo.canSpecialAttack = false;
              setTimeout(() => {
                playerTwo.canSpecialAttack = true;
              }, TIMEOUT);
              playerOne.health -= getSpecialHitPower(playerTwo);
            }
          }
        
      }

    }
    
    function keyUp(event) {

      switch(event.code) {

        case controls.PlayerOneAttack:
          playerOne.canAttack = true;
          break;

        case controls.PlayerTwoAttack:
          playerTwo.canAttack = true;
          break;

        case controls.PlayerOneBlock:
          playerOne.blocked = false;
          break;

        case controls.PlayerTwoBlock:
          playerTwo.blocked = false;
          break;

        default:
          if (controls.PlayerOneCriticalHitCombination.includes(event.code)) {
            playerOne.combination.delete(event.code);
          } else if (controls.PlayerTwoCriticalHitCombination.includes(event.code)) {
            playerTwo.combination.delete(event.code);
          }

      }

    }
    
    document.onkeydown = keyDown;
    document.onkeyup = keyUp;

    function ko(player) {
      document.onkeydown = null;
      document.onkeyup = null;
      resolve(player._id === secondFighter._id ? firstFighter : secondFighter);
    }

  });
}

export function getDamage(attacker, defender) {
  return Math.max(0, getHitPower(attacker) - getBlockPower(defender));
}

export function getHitPower(fighter) {
  const criticalHitChance = 1 + Math.random();
  return fighter.attack * criticalHitChance;
}

export function getSpecialHitPower(fighter) {
  return fighter.attack * 2;
}

export function getBlockPower(fighter) {
  const dodgeChance = 1 + Math.random();
  return fighter.defense * dodgeChance;
}
